package com.springdata.controller;

import com.springdata.model.Customer;
import com.springdata.repo.CustomerRepository;

import java.util.List;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by MiguelGomes on 7/25/16.
 */
@RestController
@RequestMapping(value = "/customer")
@Transactional(readOnly = true)
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @RequestMapping(method = RequestMethod.GET, value = "")
    public HttpEntity<List<Customer>> getCustomers(@PageableDefault(page = 0, size = 12) Pageable pageable) {
        return new HttpEntity(customerRepository.findAll(pageable));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity getCustomers(@PathVariable("id") Customer customer) {

        if (customer == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(customer);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity deleteByLastName(@RequestParam("lastName") String lastName) {
        customerRepository.deleteByLastName(lastName);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/loadFakeData")
    public ResponseEntity loadFakeData(@RequestParam(value = "quantity", defaultValue = "1")  int quantity) {


        IntStream.range(0, quantity)
                .forEach(x -> customerRepository.save(new Customer("Miguel" + x, "Gomes")));

        return ResponseEntity.ok().build();
    }
}
