package com.springdata.repo;

import com.springdata.model.Customer;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by MiguelGomes on 7/25/16.
 */
@Transactional(readOnly = true)
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    /*
    * @Modifying is for telling spring-data-jpa that this query is an
    * update operation and it requires executeUpdate() not execcuteQuery()
    * */
    @Transactional
    @Modifying
    @Query("delete Customer c where c.lastName = ?1")
    void deleteByLastName(String lastName);

    /**
     * Query constructions
     */
    List<Customer> findByLastName(String lastName);

    //Distinct Example
    List<Customer> findDistinctCustomerByLastName(String lastName);

    //IgnoreCase example
    List<Customer> findByLastNameIgnoreCase(String lastName);

    //Ordering
    List<Customer> findByLastNameOrderByFirstNameAsc(String lastName);

    //Foreign Key
    List<Customer> findByAddressesZipCode(String zipCode);

}
